# Ansible role for Mailman 2 installation

## Introduction

Mailman 2 is an older Mailing-List manager. This role may be useful to
manage legacy installations or while Mailman 3 is stabilizing.

This role installs and configure the server.

This role also expects to work hand-in hand with the OSAS postfix role
using the 'with_mailman2' setting. Alternatively you may use the
generated alias file (/etc/mailman/aliases) directly in your Postfix
rules.

This role also expects to work hand-in hand with the OSAS httpd role
(ansible-role-ah-httpd). You may call this role with the `httpd` role
variable for the vhost, or even simpler call it right after you setup
the vhost.

Here is a complete example:
```
    - name: Install ML web vhost
      include_role:
        name: httpd
        tasks_from: vhost
    - name: Install Mailman 2
      include_role:
        name: mailman2
    - name: Install Postfix
      include_role:
        name: postfix
      vars:
        myhostname: "{{ inventory_hostname }}"
        relay_domains:
          - lists.example.com
        with_mailman2: true
        aliases:
          root: admin@example.com
          listmaster: root
```

## Variables

- **mailman_pass**: site-wide administrative password

