#!/bin/bash
DIR=/var/backups/mailman/
[ -d $DIR ] || mkdir -p $DIR

for i in $(/usr/lib/mailman/bin/list_lists -b); do
    for j in admins members owners; do
        /usr/lib/mailman/bin/list_${j} > $DIR/${i}.${j}
    done
    /usr/lib/mailman/bin/config_list -o $DIR/${i}.config ${i}
done
